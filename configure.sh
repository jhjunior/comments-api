#/bin/bash

docker-compose down 

docker-compose up --detach --build db
wait 5
docker-compose up --detach --build web

curl --location --request POST 'http://localhost:8080/comment' \
--data-raw '{
    "ID": 1,
    "Email": "desafio@example.com",
    "Comm": "Primeiro comentário"
}'

# open http://localhost:8080/comments

open https://documenter.getpostman.com/view/10869420/SzYevvY5?version=latest