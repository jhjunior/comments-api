package commentrepository

import (
	"time"
	"comments/models"
	"database/sql"
	"log"
)

// CommentRepository comments
type CommentRepository struct{}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func trimQuotes(s string) string {
    if len(s) >= 2 {
        if c := s[len(s)-1]; s[0] == c && (c == '"' || c == '\'') {
            return s[1 : len(s)-1]
        }
    }
    return s
}

// GetComments comments
func (b CommentRepository) GetComments(db *sql.DB, comment models.Comment, comments []models.Comment, limit int, page int) ([]models.Comment, error)  {
	
	offset := limit * (page - 1)

	//rows, err := db.Query("select * from comments")
	
	SQL := `SELECT * FROM comments ORDER BY createdat DESC OFFSET $1 LIMIT $2`
	rows, err := db.Query(SQL, offset, limit)

	if err != nil {
		return []models.Comment{}, err
	}

	for rows.Next() {
		err = rows.Scan(&comment.ID, &comment.Email, &comment.Comm, &comment.CreatedAt)
		comments = append(comments, comment)
	}

	if err != nil {
		return []models.Comment{}, err
	}

	return comments, nil
}

// GetComment comment
func (b CommentRepository) GetComment(db *sql.DB, comment models.Comment, id int) (models.Comment, error) {
	rows := db.QueryRow("select * from comments where id=$1", id)

	err := rows.Scan(&comment.ID, &comment.Email, &comment.Comm, &comment.CreatedAt)
	if err != nil {
		return models.Comment{}, err
	}

	return comment, nil
}

// AddComment comment
func (b CommentRepository) AddComment(db *sql.DB, comment models.Comment) (int, error) {
	
	err := db.QueryRow("insert into comments (email, comm, createdat) values($1, $2, $3) RETURNING id;",
		comment.Email, comment.Comm, time.Now()).Scan(&comment.ID)

	if err != nil {
		return 0, err
	}

	return comment.ID, nil
}

// UpdateComment comment
func (b CommentRepository) UpdateComment(db *sql.DB, comment models.Comment) (int64, error) {
	log.Printf("teste %v", comment.Email)

	result, err := db.Exec("update comments set email=$1, comm=$2, createdat=$3 where id=$4 RETURNING id",
		comment.Email, comment.Comm, time.Now(), comment.ID)

	if err != nil {	
		//log.Printf("update: %v", err)
		return 0, err
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		//log.Printf("update: %v", err)
		return 0, err
	}

	return rowsUpdated, nil
}

// RemoveComment comment
func (b CommentRepository) RemoveComment(db *sql.DB, id int) (int64, error) {
	result, err := db.Exec("delete from comments where id = $1", id)

	if err != nil {
		return 0, err
	}

	rowsDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsDeleted, nil
}
