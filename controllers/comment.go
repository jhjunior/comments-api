package controllers

import (	
	"strings"
	"fmt"
	"database/sql"
	"encoding/json"
	"comments/models"
	"comments/utils"
	"comments/repository/comment"
	"log"
	"net/http"
	"strconv"

	"github.com/davecgh/go-spew/spew"

	"github.com/gorilla/mux"
)

var comments []models.Comment

// Controller comment
type Controller struct{}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func limit(x string) int {
	if x == "" {
		return 20
	}		
	limit, err := strconv.Atoi(x)
    if err != nil { log.Printf("Error parsing page: %s", err) }
	return limit
}

func page(x string) int {
	if x == "" {
		return 0
	} 
	page, err := strconv.Atoi(x)
    if err != nil { log.Printf("Error parsing page: %s", err) }
	return page
}

func arrToString(strArray []string) string {
	return strings.Join(strArray," ")
  }


// GetComments comment
func (c Controller) GetComments(db *sql.DB) http.HandlerFunc {
	

	return func(w http.ResponseWriter, r *http.Request) {
		var comment models.Comment
		var error models.Error

		fmt.Println("GET params were:", r.URL.Query())

		if err := r.ParseForm(); err != nil {
			log.Printf("Error parsing form: %s", err)
			return
		}

		var limit, page int

		params := r.URL.Query()

		if len(params) == 0 {
			limit = 10
			page = 1
		} else {

			l := params.Get("limit")
			fmt.Println("limit:", l)
			limit, _ = strconv.Atoi(l)

			p := params.Get("page")
			fmt.Println("limit:", p)
			page, _ = strconv.Atoi(p)
		}


		comments = []models.Comment{}
		//log.Printf("parsing page: %v", comments	)
		commentRepo := commentrepository.CommentRepository{}
		//log.Printf("parsing page: %v", commentRepo )
		comments, err := commentRepo.GetComments(db, comment, comments, limit, page)
		//log.Printf("parsing page: %v", comments	)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		// utils.SendSuccess(w, limit)
		
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, comments)
	}
}

// GetComment comment
func (c Controller) GetComment(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var comment models.Comment
		var error models.Error

		if err := r.ParseForm(); err != nil {
			log.Printf("Error parsing form: %s", err)
			return
		}

		params := mux.Vars(r)

		comments = []models.Comment{}
		commentRepo := commentrepository.CommentRepository{}

		id, err := strconv.Atoi(params["id"])

		if err != nil {
			error.Message = "Incorrect id."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		comment, err = commentRepo.GetComment(db, comment, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, comment)
	}
}

// AddComment comment
func (c Controller) AddComment(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var comment models.Comment
		var commentID int
		var error models.Error

		json.NewDecoder(r.Body).Decode(&comment)


		if comment.Email == "" || comment.Comm == "" {
			error.Message = "Enter missing fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		commentRepo := commentrepository.CommentRepository{}
		commentID, err := commentRepo.AddComment(db, comment)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, commentID)
	}
}

// UpdateComment comment
func (c Controller) UpdateComment(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var comment models.Comment
		var error models.Error

		json.NewDecoder(r.Body).Decode(&comment)

		if comment.Email == "" || comment.Comm == "" {
			error.Message = "Enter all fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		commentRepo := commentrepository.CommentRepository{}
		rowsUpdated, err := commentRepo.UpdateComment(db, comment)

		spew.Dump(err)

		if err != nil {
			log.Printf("update: %v", err)
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsUpdated)
	}
}

// RemoveComment comment
func (c Controller) RemoveComment(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)

		commentRepo := commentrepository.CommentRepository{}

		id, err := strconv.Atoi(params["id"])

		if err != nil {
			error.Message = "Incorrect id."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		rowsDeleted, err := commentRepo.RemoveComment(db, id)

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsDeleted)
	}
}
