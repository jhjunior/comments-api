package models

// Error types
type Error struct {
	Message string `json:"message"`
}
