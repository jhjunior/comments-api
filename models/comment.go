package models

import (
	"fmt"
	"time"
)

// Marshaler comment
type Marshaler interface {
    MarshalJSON() ([]byte, error)
}

// JSONTime comment
type JSONTime time.Time

// MarshalJSON comment 
func (t JSONTime)MarshalJSON() ([]byte, error) {
    //do your serializing here
    stamp := fmt.Sprintf("\"%s\"", time.Time(t).Format("Mon Jan _2"))
    return []byte(stamp), nil
}

// Comment type
type Comment struct {
	ID          int       `json:"id"`
	Email       string    `json:"email"`
	Comm	 	string    `json:"comm"`
	CreatedAt   time.Time `json:"createdat"`
}
