FROM golang:1.11

WORKDIR /go/src/comments  
COPY . . 
RUN go get -d -v ./...
RUN go install -v ./...

ADD . /comments  

RUN  go build -o comments .

EXPOSE 8080:8080

CMD ["./comments"]