package main

import (
	"database/sql"
	"fmt"
	"comments/controllers"
	"comments/driver"
	"comments/models"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/subosito/gotenv"
)

var books []models.Comment
var db *sql.DB

func init() {
	gotenv.Load()
}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	db = driver.ConnectDB()
	router := mux.NewRouter()
	controller := controllers.Controller{}

	router.HandleFunc("/comments", controller.GetComments(db)).Methods("GET")
	router.HandleFunc("/comment/{id}", controller.GetComment(db)).Methods("GET")
	router.HandleFunc("/comment", controller.AddComment(db)).Methods("POST")
	router.HandleFunc("/comment", controller.UpdateComment(db)).Methods("PUT")
	router.HandleFunc("/comment/{id}", controller.RemoveComment(db)).Methods("DELETE")

	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}

	fmt.Println("Server has strated on port", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
