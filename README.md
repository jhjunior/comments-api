# API Comentários

API de comentário desenvolvida em Go com persistência em banco de dados Postgres. A escolha de uma linguagem compilada visava atender aos requisitos de performance.

O código foi empacotado no docker para portablidade em multiplas clouds ou PaaS.

## Pre-requisitos

### docker-desktop

https://www.docker.com/products/docker-desktop

- docker desktop community 2.2.0.4 stable
- engine 19.03.8
- Compose 1.25.4

## Documentação

- https://documenter.getpostman.com/view/10869420/SzYevvY5?version=latest

## Execução

> $ sh configure.sh

## Backlog

Para evoluir a infraestrutura e a API trabalharia 

- Recurso de upvote e downvote
- Recurso para denuncia de abuso
- Deploy em um PaaS (Heroku ou Tsuru)
- Testes automatizados no código
- Integração Contínua
